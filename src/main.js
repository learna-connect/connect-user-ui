import Vue from 'vue'
import App from './App.vue'
import * as Keycloak from 'keycloak-js'
import vuetify from './plugins/vuetify';
import router from './router'
import store from './store'
import axios from 'axios'
import VueLogger from 'vuejs-logger'

Vue.config.productionTip = false

const isProduction = process.env.NODE_ENV === 'production';

const options = {
    isEnabled: true,
    logLevel : isProduction ? 'error' : 'debug',
    stringifyArguments : false,
    showLogLevel : true,
    showMethodName : true,
    separator: '|',
    showConsoleColors: true
};


Vue.use(VueLogger, options);


let initOptions = {
  url: 'http://localhost:8080/auth',
  realm: 'learna',
  clientId: 'connect-user-ui',
  onLoad:'login-required'
};


let keycloak = Keycloak(initOptions);

keycloak.init({onLoad: initOptions.onLoad})
    .then((auth) => {
      if(!auth) {
        window.location.reload();
      } else {
        //Axios defaults
        axios.defaults.baseURL = 'http://localhost:8083';

        store.commit('setTokens', {
            object: keycloak
        });

        new Vue({
          vuetify,
          router,
          store,
          render: h => h(App)
        }).$mount('#app');

        Vue.$log.info("Authenticated");
      }

        setTimeout(() =>{
            keycloak.updateToken(70).then((refreshed)=>{
                if (refreshed) {
                    Vue.$log.debug('Token refreshed'+ refreshed);
                } else {
                    Vue.$log.warn('Token not refreshed, valid for '
                        + Math.round(keycloak.tokenParsed.exp + keycloak.timeSkew - new Date().getTime() / 1000) + ' seconds');
                }
            }).catch(()=>{
                Vue.$log.error('Failed to refresh token');
            });


        }, 60000)
    })

