import Vue from 'vue';
import Vuex from 'vuex'


Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        keycloakObject: {},
        loadingIndicator:false
    },
    mutations: {
        setTokens(state, payload) {
            state.keycloakObject = payload.object
        }
    },

    getters: {
        userName: state => {
            return state.keycloakObject.tokenParsed.name;
        },
        userId: state => {
            return state.keycloakObject.subject;
        },
        accessToken: state => {
            return state.keycloakObject.token;
        }
    }

})