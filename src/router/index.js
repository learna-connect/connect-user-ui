import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import PageUnderConstruction from "../components/PageUnderConstruction";

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
    {
      path: '/account',
      name: 'Account',
      component: PageUnderConstruction
    },
    {
      path: '/appointments',
      name: 'Appointments',
      component: PageUnderConstruction
    }
]

const router = new VueRouter({
  routes
})

export default router
